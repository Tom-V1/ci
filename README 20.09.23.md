# serverless-cicd

This repository provides a skeleton with some files in order for you to get started creating your own CI-CD solution using GitLab CI/CD and to create your first AWS Lambda following a common pattern in the industry.

## Desired application diagram

```mermaid
graph LR
    GitLab(GitLab repo)--> CICD(GitLab CI/CD)
    CICD --> S3{AWS S3 Bucket}
    S3 -->Browser(Static site <br /> accessed through browser)
    API[API Gateway] -->|request| Lambda(AWS Lambda)
    Lambda -->|response| API
    Browser -->|request| API
    API -->|response| Browser
```

## Project files

- Under the `www` folder, there are two files: `index.html` and `error.html`. These are the files we will deploy for our static website on an AWS S3 Bucket.
- `your-first-lambda.py` is the Lambda function that you will deploy in AWS and eventually invoke once you deploy your static website on AWS. Modify it to include the names of your group members.
- `sample_unit_test.py` contains some sample unit tests and will serve you to test your CI job in GitLab CI/CD. One or more of the tests are failing at the moment. The framework used for the tests is [unittest](https://docs.python.org/3/library/unittest.html)


## Tasks

### Create an S3 Bucket for your static website on AWS

S3 is an [Object storage](https://cloud.netapp.com/blog/block-storage-vs-object-storage-cloud) service by AWS.
You may be wondering how is it possible that such service can host a static website. You will need to find out how you can enable your bucket to host a static website.

### Set up CI using GitLab CI/CD

GitLab comes with a built-in CI/CD tool.
It has a vast feature set.
You'll need to explore its documentation and do some research to figure out which are relevant to meet the requirements for this project.

Let's think about CI first. Ask yourself the following questions:

- What sort of steps do you think you should include in your CI/CD job? What sort of checks should you include before your application is deployed to the Cloud?
- What should trigger these checks to be run?

The next step is figuring out how to configure GitLab to do that!
Do some research and discuss the answers and potential solutions you find with your group.

:exclamation: Remember that, ultimately, the idea is for your job to pass all the necessary checks before moving to the CD task.

Here's another question to think about once you've got your checks to run:

- Where are the jobs you've implemented actually run? Who controls the machines they run on?

### Set up CD using GitLab CI/CD

Great job! Now you have a green CI job, the next step is to be able to deploy your application to the S3 bucket you created initially.

- What does deployment actually mean for this particular application? What pieces of data will need to be uploaded to S3 to make the website accessible to the user?
- Until now, you've interacted with S3 through the AWS Console, which is a human-friendly UI. But the GitLab servers are not human! Clicking around doesn't seem like the best way to do things for a machine ... Can you discover another way to interact with AWS? Do some research around the keywords `programmatic access` and `aws` and discuss what you find with your group.

:bulb: Here's another thing to consider: By default, AWS doesn't just give anyone permission to write to an S3 bucket that they don't own. Who/what will need permission to write to your S3 bucket? How can you get AWS to trust it? The keyword `credentials` might help.

:information_source: You might have thought of just configuring your S3 bucket to allow anyone to write to it. Wouldn't that solve the problem neatly! Why is it not the best solution? 

<details>
  <summary markdown="span">:thinking_face: <b>If you've done the research and had a go but are still stuck, click here!</b></summary>

  So far you've used the AWS console to interact with S3 and other AWS services.
  As mentioned, the UI isn't convenient to use from a machine.
  Wouldn't it be cool if we could somehow issue commands to interact with S3 instead of having to click around?
  There is such a tool and it's called the [AWS Command Line Interface](https://aws.amazon.com/cli/) (CLI).
  With the right credentials, it allows issuing commands from a terminal to interact with AWS services.

  :bulb: Install the AWS CLI on your laptop and try to issue some commands. For example, you could try and find a command that lists all the files that are currently in your S3 bucket. Does it work?

  The personal users that have been created for you on AWS only have console access.
  This means that you can only interact with AWS services via the UI.
  To use the AWS CLI, you're going to need an IAM user that has something called "programmatic access" to AWS.

  :information_source: You can configure a user's level of access when creating new users via the [IAM console](https://console.aws.amazon.com/iamv2/home?#/users).

  Once you have the user, you're going to somehow need to pass this user's credentials to your CD job running on the GitLab servers so that it can identify itself to AWS.

  And finally, you'll have to craft the AWS CLI command that will copy the right data from your GitLab repo into your S3 bucket!
</details>

### AWS Lambda

Before moving on to this task, make sure you have successfully deployed your `html` files to your S3 Bucket.

- On the AWS Console, access the AWS Lambda service and create a Python Lambda function whose definition matches the one in the `your-first-lambda.py` file. Do not forget to modify it!

Great job! You've created your first Lambda function on AWS, how exciting! Now you may be wondering, okay, how do we `invoke` it?

### API Gateway

You've reached the last task of the week project, congratulations on the hard and great work! You have a general idea about the purpose of API Gateway from the session on Monday and the diagram. We will use API Gateway on AWS to invoke our Lambda function, however:

- Is this the only way of invoking a Lambda function?
- What are the advantages of using API Gateway?
- Could you invoke a Lambda function directly?

The final task is to create an API Gateway `endpoint` that you can consume in order to invoke your Lambda function.

:bulb: Explore the `www/index.html` file. Can you spot which type of request you should perform?

For an extra stretch, feel free to add additional settings like `metrics` and `logging` to your API Gateway endpoint. You can monitor these on `AWS CloudWatch`.

## Bonus

Think about what could have been improved in this project:
- Is it considered best practice to add both the CI and CD bits in one single script?
- Is there another way in which you could have created the API Gateway and your Lambda function rather than manually through the AWS Console?

Finally, if you have some extra time this week, start reading and getting familiar with Infrastructure Management and Orchestration (using `Terraform`). What benefits do you see it can bring to a larger project or even to this one?

### Supporting Materials

- [GitLab CI/CD](https://docs.gitlab.com/ee/ci/)
- [Using AWS Lambda with Amazon API Gateway](https://docs.aws.amazon.com/lambda/latest/dg/services-apigateway.html)

### Additional Resources

- [AWS S3 Bucket](https://aws.amazon.com/s3/)
- [Terraform](https://www.terraform.io/docs/index.html)
